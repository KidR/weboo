const game = require('./game')
const EVENTS = require('./metadata/events.json')
const CARDS = require('./metadata/cards.json')
const GAMEDATA = require('./metadata/gamedata.json')
const config = require('./.config.json')
const express = require('express')
const etherealColor = require('ethereal-color')
const huegroup = require('./lib/huegroup')
const hue = require('./lib/hue')
const app = express()
const port = 9000
const ONE_SECOND = 1000

const _log = ({ prefix, data, event, msg }) => {
  if (config.onDebug) {
    let log = prefix.toUpperCase();
    if (event !== undefined) log += ` on ${event}`;
    log += ":";
    if (msg !== undefined) log += ` ${msg}\n`;
    if (data !== undefined) console.log(log, data);
    else console.log(log);
  }
};

const logServer = ({ data, event, msg }) => {
  _log({ prefix: "SERVER", data: data, event: event, msg: msg });
};

const logClient = ({ data, event, msg, socket }) => {
  _log({
    prefix: "CLIENT",
    data: data,
    event: `${event} - socketId=${socket.id}`,
    msg: msg,
  });
};

const server = app.listen(port, () => {
  logServer({ msg: `listening at http://localhost:${port}` });
});

const io = require("socket.io")(server, {
  cors: {
    origin: "*"
  }
});

// app.use(cors())

app.get("/", function(req, res) {
  res.send("Hello World!");
});

const getWordTypeByCard = (card) => {
  switch (card) {
    case CARDS.red.id:
      return game.GameData.TYPE_WORDS_TABOO;
    default:
      return game.GameData.TYPE_WORDS;
  }
};

const setNewWord = (gameData, data) => {
  const wordType = getWordTypeByCard(data.game.cardType);
  try {
    data.game.word = gameData.generateWord(wordType);
  } catch (error) {
    if (error instanceof RangeError) {
      logServer({
        event: error.name,
        msg: `${error.message} => Reseting words.`,
      });
      gameData.resetWords();
      data.game.word = gameData.generateWord(wordType);
    }
  }
  if (gameData.currentType != game.GameData.TYPE_WORDS) {
    data.game.options.taboo = data.game.word[1];
    data.game.word = data.game.word[0];
  }
}

const COLOR_WHITE = "#FFFFFF"
const DEFAULT_BRI = hue.HUE_STATES.BRI.max
const HUE_GROUP_ID = '0'
const BEGIN_TRANSITION_COUNTDOWN = 20
let gameData = new game.GameData()
let timer = null
let bri = DEFAULT_BRI

const changeHueGroupsColor = (color, bri, second = hue.HUE_STATES.TRANSITION_TIME.default / 10) => {
  const rgb = etherealColor.Color(color).get('rgb').object
  const xyBri = hue.rgbToXY(rgb.r, rgb.g, rgb.b)
  huegroup.setGroupStates(HUE_GROUP_ID, {
    [hue.HUE_STATES.ON.field]: true,
    [hue.HUE_STATES.XY.field]: [xyBri.x, xyBri.y],
    [hue.HUE_STATES.BRI.field]: bri,
    [hue.HUE_STATES.TRANSITION_TIME.field]: hue.secondToTransitionTime(second)
  }).then((response) => logServer({ msg: response }))
}
const changeHueGroupsBri = (bri) => {
  huegroup.setGroupStates(HUE_GROUP_ID, {
    [hue.HUE_STATES.BRI.field]: bri
  }).then((response) => logServer({ msg: response }))
}

io.on("connection", function(socket) {
  socket.on(EVENTS.CLIENT.INGAME, function(data) {
    data.game.countdown = data.game.duration;
    // USE HUE API
    if (data.settings.activateHueAPI)
      changeHueGroupsColor(data.params.color, bri)
    timer = setInterval(() => {
      if (data.game.countdown > 0) {
        data.game.countdown--
        io.emit(EVENTS.SERVER.SEND_TIMER, data.game.countdown)
        if (data.game.countdown === BEGIN_TRANSITION_COUNTDOWN && data.settings.activateHueAPI)
          changeHueGroupsColor(COLOR_WHITE, bri, BEGIN_TRANSITION_COUNTDOWN)
        if (data.game.countdown === 0) {
          data.game.score = gameData.score
          io.emit(EVENTS.SERVER.SEND_SCORE, data)
          clearInterval(timer)
          return;
        }
      }
      return;
    }, ONE_SECOND);
    setNewWord(gameData, data)
    data.game.options = Object.assign(data.game.options, CARDS[data.game.cardType].options)
    io.emit(EVENTS.SERVER.INIT_INGAME, data)
    logClient({ event: EVENTS.CLIENT.INGAME, data: data, socket: socket })
  })

  socket.on(EVENTS.CLIENT.ENDGAME, function (data) {
    clearInterval(timer)
    bri = DEFAULT_BRI
    if (data.game.countdown > 0 && data.settings.activateHueAPI)
      changeHueGroupsColor(COLOR_WHITE, bri)
    gameData.resetScore()
    gameData.resetOptions()
    data = Object.assign(data, { ingame: false, params: {}, game: gameData })
    io.emit(EVENTS.SERVER.MENU_SELECT, data)
    logClient({ event: EVENTS.CLIENT.ENDGAME, data: data, socket: socket })
  })

  socket.on(EVENTS.CLIENT.VALIDATE_WORD, function(data) {
    data.params.valid
      ? gameData.validate(data.game.word)
      : gameData.skip(data.game.word);
    setNewWord(gameData, data);
    io.emit(EVENTS.SERVER.SEND_WORD, data);
    logClient({
      event: EVENTS.CLIENT.VALIDATE_WORD,
      data: data,
      socket: socket,
    });
  });

  socket.on(EVENTS.CLIENT.DECREMENT_HINT, function (data) {
    if (data.game.options.startCounter <= 1) {
      io.emit(EVENTS.SERVER.SEND_SCORE, Object.assign(data, { game: gameData }))
      clearInterval(timer)
    } else {
      bri -= 12
      if (data.settings.activateHueAPI)
        changeHueGroupsBri(bri)
      data.game.options.startCounter--
      io.emit(EVENTS.SERVER.SEND_HINT_COUNTER, data)
    }
    logClient({ event: EVENTS.CLIENT.DECREMENT_HINT, data: data, socket: socket })
  })

  socket.on(EVENTS.CLIENT.RESET_GAME, function (data) {
    clearInterval(timer)
    bri = DEFAULT_BRI
    if (data.settings.activateHueAPI)
      changeHueGroupsColor(COLOR_WHITE, bri)
    gameData = new game.GameData()
    io.emit(EVENTS.SERVER.RESET_GAME, Object.assign(GAMEDATA, { settings: data.settings }))
    logClient({ event: EVENTS.CLIENT.RESET_GAME, data: gameData, socket: socket })
  })

  socket.on("disconnect", (reason) => {
    clearInterval(timer);
    logServer({ event: "DISCONNECT", msg: reason });
  });
});
