const fetch = require('node-fetch');
const huebridge = require("./huebridge");
const bridge = huebridge.devBridge();

const API_RESOURCE = "scenes";

module.exports = {
  /**
   * Retrieve all scenes
   *
   * Note: scene lightstates are available only when retriving
   *       a single scene.
   */
  fetchScenes: async function () {
    const response = await fetch(bridge.access(`/${API_RESOURCE}/`), {
      method: "GET",
      headers: { "Content-Type": "application/json" }
    });
    return await response.json();
  },
  /**
 * Retrieve a scene by id
 *
 * Note: scene lightstates are available only when retriving
 *       a single scene.
 */
  fetchScene: async function (id) {
    const response = await fetch(bridge.access(`/${API_RESOURCE}/${id}`), {
      method: "GET",
      headers: { "Content-Type": "application/json" }
    });
    return await response.json();
  },
  /**
 * Set scene lighstates
 *
 * @param {String} id
 * @param {Object} states
 */
  setSceneStates: async function (id, states) {
    const response = await fetch(bridge.access(`/${API_RESOURCE}/${id}/state/`), {
      method: "PUT",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(states)
    });
    return await response.json();
  },
  /**
 * Filter scenes entries by a scene's property.
 * Check HueAPI to get more information about scene and its properties.
 *
 * @param {Array} scenes
 * @param {String} property
 * @param {String} value
 *
 * @returns {Object} scenesFiltered
 */
  filterScenesBy: function (scenes, property, value) {
    /*Entries format: [[<sceneId: String>,<sceneData: Object>], ...]     */
    return Object.fromEntries(
      scenes.filter(subarray => subarray[1][property] === value.toString())
    );
  }

}







