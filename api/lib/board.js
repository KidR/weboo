
const cards = Object.keys(require('../metadata/cards.json'))
const DEFAULT_BOARD_LENGTH = 30

/**
 * Generate a number between 0 and max-1
 * 
 * @param {Integer} max 
 */
const randInt = (max) => Math.floor(Math.random() * Math.floor(max))

module.exports = {
    /**
     * Generate a board randomly
     * 
     * @param {Integer} n 
     */
    generateBoard: (n=DEFAULT_BOARD_LENGTH) => Array(n).fill(0).map(x => cards[randInt(cards.length)]),
    /**
     * Load board from JSON file boards.json
     */
    loadBoard: undefined,
    /**
     * Export board in JSON file boards.json
     */
    exportBoard: undefined
}