const fetch = require('node-fetch');
const huebridge = require("./huebridge");
const bridge = huebridge.devBridge();

const API_RESOURCE = "groups";

module.exports = {
  HUE_GROUP_TYPES: {
    ENTERTAINMENT: "Entertainment",
    ROOM: "Room",
    ZONE: "Zone"
  },
  /**
 * Set group state
 *
 * @param {String} id
 * @param {Object} states
 */
  setGroupStates: async function (id, states) {
    const response = await fetch(
      bridge.access(`/${API_RESOURCE}/${id}/action/`),
      {
        method: "PUT",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify(states)
      }
    );
    return await response.json();
  }
}