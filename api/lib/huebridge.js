class HueBridge {
  constructor(ip, userkey) {
    this.ip = ip;
    this.userkey = userkey;
    this.url = `http://${this.ip}/api/${this.userkey}`;
  }

  access(resource) {
    return `${this.url}${resource}`;
  }
}

module.exports = {
  HueBridge,
  devBridge: function() {
    return new HueBridge(
      "192.168.1.10",
      "jEYmOuH2SscKaqszYrUpHODfwDBnYW5taQVhjNqA"
    );
  }
}