const fetch = require('node-fetch');
module.exports = {
  HUE_STATES: {
    BRI: {
      field: "bri",
      max: 254,
      min: 1
    },
    SAT: {
      field: "sat",
      max: 254,
      min: 0
    },
    HUE: {
      field: "hue",
      max: 65535,
      min: 0
    },
    XY: {
      field: "xy",
      max: 1.0,
      min: 0.0
    },
    ON: {
      field: "on"
    },
    EFFECT: {
      field: "effect",
      default: "none"
    },
    TRANSITION_TIME: {
      field: "transitiontime",
      default: 4
    }
  },
  /**
 * Convert RGB colors into XY colors
 *
 * @param {Integer} r
 * @param {Integer} g
 * @param {Integer} b
 *
 * @returns {Object} xybri
 */
  rgbToXY: function (r, g, b) {
    r = r / 255;
    g = g / 255;
    b = b / 255;
    // gamma correction
    r = r > 0.04045 ? Math.pow((r + 0.055) / (1.0 + 0.055), 2.4) : r / 12.92;
    g = g > 0.04045 ? Math.pow((g + 0.055) / (1.0 + 0.055), 2.4) : g / 12.92;
    b = b > 0.04045 ? Math.pow((b + 0.055) / (1.0 + 0.055), 2.4) : b / 12.92;
    let X = r * 0.649926 + g * 0.103455 + b * 0.197109;
    let Y = r * 0.234327 + g * 0.743075 + b * 0.022598;
    let Z = r * 0.0 + g * 0.053077 + b * 1.035763;
    let x = X / (X + Y + Z);
    let y = Y / (X + Y + Z);
    return { x: x, y: y, bri: Math.round(Y * 255) };
  },

  /**
   * Convert XY colors into RGB colors
   *
   * @param {Float} x
   * @param {Float} y
   * @param {Integer} bri
   *
   * @returns {Object} rgb
   */
  xyToRGB: function (x, y, bri) {
    x = parseFloat(x);
    y = parseFloat(y);
    let z = parseFloat(1.0 - x - y);
    let Y = bri / HUE_STATES.BRI.max;
    let X = (Y / y) * x;
    let Z = (Y / y) * z;
    let r = X * 1.656492 - Y * 0.354851 - Z * 0.255038;
    let g = -X * 0.707196 + Y * 1.655397 + Z * 0.036152;
    let b = X * 0.051713 - Y * 0.121364 + Z * 1.01153;
    r =
      r <= 0.0031308 ? 12.92 * r : (1.0 + 0.055) * Math.pow(r, 1.0 / 2.4) - 0.055;
    g =
      g <= 0.0031308 ? 12.92 * g : (1.0 + 0.055) * Math.pow(g, 1.0 / 2.4) - 0.055;
    b =
      b <= 0.0031308 ? 12.92 * b : (1.0 + 0.055) * Math.pow(b, 1.0 / 2.4) - 0.055;
    let maxValue = Math.max(r, g, b);
    const fixColorValue = c => (c > 255 ? 255 : c);
    r = fixColorValue(Math.abs((r / maxValue) * 255));
    g = fixColorValue(Math.abs((g / maxValue) * 255));
    b = fixColorValue(Math.abs((b / maxValue) * 255));
    return {
      r: r,
      g: g,
      b: b
    };
  },
  ctToXY: function (ct) {
    let T = 1000000 / ct;
    let x = 0;
    if (0 <= T && T <= 3000) {
      x =
        0.244063 +
        (0.09911 * Math.pow(10, 3)) / T +
        (2.9678 * Math.pow(10, 6)) / Math.pow(T, 2) +
        (-4.607 * Math.pow(10, 9)) / Math.pow(T, 3);
    } else if (3000 < T && T <= 21000) {
      x =
        0.23704 +
        (0.24748 * Math.pow(10, 3)) / T +
        (1.9018 * Math.pow(10, 6)) / Math.pow(T, 2) +
        (-2.0064 * Math.pow(10, 9)) / Math.pow(T, 3);
    }
    let y = -3.0 * Math.pow(x, 2) + 2.87 * x - 0.275;
    return { x: x, y: y };
  },
  /**
 * Convert alpha value [0.0,1.0] to bri values [HUE_STATES.BRI.min,HUE_STATE.BRI.max]
 *
 * @param {Float} alpha
 *
 * @returns {Integer} bri
 */
  alphaToBri: function (alpha) {
    return Math.ceil(alpha * HUE_STATES.BRI.max);
  },
  /**
 * Convert bri values [HUE_STATES.BRI.min,HUE_STATE.BRI.max] to alpha value [0.0,1.0]
 *
 * @param {Integer} bri
 *
 * @returns {Float} alpha
 */
  briToAlpha: function (bri) {
    return parseFloat(bri / HUE_STATES.BRI.max);
  },
  secondToTransitionTime: function (s) {
    return s * 10;
  }
}
