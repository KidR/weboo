const path = require('path');
const yaml = require('js-yaml');
const fs = require('fs');
const DIR_WORDS = "./data/";
const FILE_WORDS = "words.data";
const FILE_TABOO_WORDS = "taboo.yml";

const loadSimpleWords = () => {
  const pathFile = path.join(DIR_WORDS, FILE_WORDS);
  return fs.readFileSync(pathFile, { encoding: 'utf8', flag: 'r' }).split("\n");
}

const loadTabooWords = () => {
  const pathFile = path.join(DIR_WORDS, FILE_TABOO_WORDS);
  const words = Object.entries(yaml.load(fs.readFileSync(pathFile, { encoding: 'utf8', flag: 'r' })));
  return words;
}

const randInt = (max) => Math.floor(Math.random() * Math.floor(max));

loadTabooWords();

class GameData {

  static TYPE_WORDS = "simple";
  static TYPE_WORDS_TABOO = "taboo";

  constructor() {
    this.score = {
      validated: [],
      skipped: []
    }
    this.used = []
    this.currentType = ""
    this.currentWord = ""
    this.currentIndex = 0
    this.options = {}
    this.words = {
      simple: loadSimpleWords(),
      taboo: loadTabooWords()
    }
  }

  resetScore() {
    this.score = {
      validated: [],
      skipped: []
    }
  }

  resetWords() {
    this.words = {
      simple: loadSimpleWords(),
      taboo: loadTabooWords()
    }
  }

  resetOptions(){
    this.options = {}
  }

  resetGame() {
    this.resetScore()
    this.resetWords()
    this.used = []
    this.currentType = ""
    this.currentWord = ""
    this.currentIndex = 0
    this.options = {}
  }

  generateWord(wordGame) {
    this.currentType = wordGame
    this.currentIndex = randInt(this.words[this.currentType].length)
    this.currentWord = this.words[this.currentType].splice(this.currentIndex, 1).shift()
    if (this.currentWord === undefined) {
      throw RangeError(`All words from '${this.currentType}' type were consume`)
    }
    this.used.push(this.currentWord)
    return this.currentWord
  }

  validate(word) {
    this.score.validated.push(word)
  }

  skip(word) {
    this.score.skipped.push(word)
  }

  get word() {
    return this.currentWord
  }

}

module.exports = {
  GameData
}