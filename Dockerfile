FROM node:latest as build-stage
WORKDIR /webooxxl
COPY package*.json ./
RUN npm install -g npm@7.5.4
RUN npm install
COPY ./ .
RUN npm run build

FROM nginx as production-stage
RUN mkdir /webooxxl
COPY --from=build-stage /webooxxl/dist /webooxxl
COPY nginx.conf /etc/nginx/nginx.conf