import Vue from "vue";
import App from "./App.vue";
import vuetify from "./plugins/vuetify";
import UUID from "vue-uuid";
import VueLogger from "vuejs-logger";

const isProduction = process.env.NODE_ENV === "production";

const options = {
  isEnabled: true,
  logLevel: isProduction ? "error" : "debug",
  stringifyArguments: false,
  showLogLevel: true,
  showMethodName: false,
  separator: ":",
  showConsoleColors: true
};

Vue.use(VueLogger, options);
Vue.use(UUID);

Vue.config.productionTip = false;

new Vue({
  vuetify,
  render: h => h(App)
}).$mount("#app");
